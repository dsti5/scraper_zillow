import clipboard
import pyautogui
import numpy as np
import cv2

import pytesseract

pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'


class MyScraper:

    def get_position(self):
        """
        Gets the mouse coordinates
        return: the coordinates
        """
        return pyautogui.position()

    def get_screenshot(self):
        """
        Gets coordinates and "screenshot" accordingly
        return: the crop image
        """

        # We wait for any the user input to put his mouse on the wanted spot
        # Get the starting point from the mouse position
        input('Input something when you are ready for the top left corner\n')
        tlx, tly = pyautogui.position()
        print(tlx)
        print(tly)
        # Get the starting point from the mouse position
        input('Input something when you are ready for the bottom right corner\n')
        rbx, rby = pyautogui.position()
        print(rbx)
        print(rby)

        return self.extract_crop(tlx=tlx, tly=tly, rbx=rbx, rby=rby)

    def extract_crop(self, tlx, tly, rbx, rby):
        """
        Extract a crop
        :param tlx: int x top left position
        :param tly: int y top left position
        :param rbx: int x bottom right position
        :param rby: int y bottom right position
        :return: the crop
        """
        image = pyautogui.screenshot()
        # We change the image color
        image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
        return image[tly:rby, tlx:rbx, :3]  # image[ymin:ymax,xmin:xmax]

    def image_to_text(self, crop):
        """
        transform an image with text into a string
        :param crop : an image
        :return: a string
        """
        text_from_crop = pytesseract.image_to_string(crop)
        return text_from_crop

    def return_text_from_webpage(self):
        """
        return the text of a current page url
        execute a ctrl+a ctrl+c
        :return:
        """
        pyautogui.moveTo(1529, 320)
        pyautogui.click()
        pyautogui.hotkey('ctrl', 'a')
        pyautogui.hotkey('ctrl', 'c')
        return clipboard.paste()
