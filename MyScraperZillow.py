import pyautogui

from MyScraper import MyScraper


class MyScraperZillow(MyScraper):

    def extract_zillow_address(self):
        """
        Extract the address from the Zillow website
        based on the user screen size
        :return:
        """
        assert pyautogui.size() == (1920, 1080)
        return self.extract_crop(tlx=1133,
                                 tly=297,
                                 rbx=1700,
                                 rby=340)

    def zillow_address_to_text(self):
        """
        screenshot the address and return the text
        """
        crop_address = self.extract_zillow_address()
        return self.image_to_text(crop_address)

    def get_best_schools(self, fulltext):
        """
        return a list of the best school close to the given house
        :return: list of tuples
        """
        schools = fulltext[
                  fulltext.find("Nearby schools in San Diego"):fulltext.find("About GreatSchools")].splitlines()
        try:
            return [(schools[2].strip(), schools[3].strip()),
                    (schools[6].strip(), schools[7].strip()),
                    (schools[10].strip(), schools[11].strip())
                    ]
        except:
            return []
