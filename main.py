from MyScraperZillow import MyScraperZillow

if __name__ == '__main__':
    regex_grade = r'[^0-9]*(10|[1-9]).*'

    # instantiate my scaper
    myscraper_zillow = MyScraperZillow()

    # get the address
    address = myscraper_zillow.zillow_address_to_text()
    print("output:" + address)

    text = myscraper_zillow.return_text_from_webpage()
    schools = myscraper_zillow.get_best_schools(text)
    print(schools)
